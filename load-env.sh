#!/bin/bash

echo "Creating environment variables"

export NAME="Dilsa"
export LASTNAME="Barrientos"

echo "Loading variables into profile"

sudo echo "export NAME=${NAME}" >> /etc/profile.d/owner.sh
sudo echo "export LASTNAME=${LASTNAME}" >> /etc/profile.d/owner.sh

echo "Checking environment variables"

if [ -z $NAME ]; then 
	echo "NAME not found"
else 
	echo "NAME=${NAME}"
fi

if [ -z $LASTNAME ]; then
        echo "LASTNAME not found"         
else    
        echo "LASTNAME=${LASTNAME}" 
fi


