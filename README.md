## Virtualization of environments with Vagrant

**Vagrant** is an **open source** tool for **creating and configuring reproducible and sharable virtualized environments** in a very simple way. It creates and configures virtual machines from simple configuration files called **_Vagrantfile_**.

Vagrant is available for **different operating systems** such as Windows, MacOS X and GNU/Linux, 

It works by default with **VirtualBox** and is able to work with **multiple vendors** such as VMware, AWS (Amazon Web Services) and others.

Virtual machines created with Vagrant can be used in **development environments** and later deployed in **production environments**.

Vagrant is **operated from the command line** using the command **_vagrant_** followed by the operation to be executed (_up, ssh, halt, status_ ...).

> NOTE: This document has been made for virtualization environments on and with Ubuntu 22.04.


#### Virtual machine with default configuration

Once VirtualBox and Vagrant are installed, a virtual machine with default configuration can be created using the following sequence of instructions:
````
# Create the virtual machine
vagrant init

# Start the virtual machine
vagrant up
````

#### Access to the virtual machine

The virtual machine is accessed from a terminal, by command line, by first going to the directory where it is installed and then using the _vagrant_ instruction with the _ssh_ command:
```` 
cd <directory_name>
vagrant ssh
```` 
