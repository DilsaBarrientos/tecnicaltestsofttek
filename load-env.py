#!/usr/bin/python3
import subprocess
import os

name = "Dilsa"
lastname = "Barrientos"

print("Creating environment variables")

command = 'export NAME="' + name + '"' 
p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
command = 'export LASTNAME="' + lastname + '"' 
p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True) 

print("Loading variables into profile")

command = 'sudo echo "export NAME=' + name + '"  >> /etc/profile.d/owner.sh'
p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
command = 'sudo echo "export LASTNAME=' + lastname + '" >> /etc/profile.d/owner.sh' 
p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)

