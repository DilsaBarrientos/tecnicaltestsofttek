#!/usr/bin/python3
import os

print("Checking environment variables")

if "NAME" not in os.environ:
    print("NAME not found")
else:
    print("NAME=" + os.environ.get('NAME'))

if "LASTNAME" not in os.environ:
    print("LASTNAME not found")        
else:
    print("LASTNAME=" + os.environ.get('LASTNAME'))

